# Incremental Risk Charge Methodology

The Basel Committee on Banking Supervision (see Basel [2009 a]) released the new guidelines for Incremental Risk Charge (IRC) that are part of the new rules developed in response to the financial crisis and is a key part of a series of regulatory enhancements being rolled out by regulators.

IRC supplements existing Value-at-Risk (VaR) and captures the loss due to default and migration events at a 99.9% confidence level over a one-year capital horizon. The liquidity of position is explicitly modeled in IRC through liquidity horizon and constant level of risk (see Xiao[2017]).

The constant level of risk assumption in IRC reflects the view that securities and derivatives held in the trading book are generally more liquid than those in the banking book and may be rebalanced more frequently than once a year (see Aimone [2018]).  IRC should assume a constant level of risk over a one-year capital horizon which may contain shorter liquidity horizons. This constant level of risk assumption implies that a bank would rebalance, or rollover, its positions over the one-year capital horizon in a manner that maintains the initial risk level, as indicated by the profile of exposure by credit rating and concentration.

The current market risk capital rule is:
Total market risk capital = general market risk capital 
     + basic specific risk capital				(1)
     + specific risk surcharge
where
	General market risk capital = 3 x  
	Basic specific risk capital = 3 x  
	Specific risk surcharge = (m – 3) x  
where m is the specific risk capital multiplier under regulators’ guidance

The new market risk capital standard will be:
	  Total market risk capital = general market risk capital 
+ basic specific risk capital				(2)
        + incremental risk charge
where Incremental risk charge =  

The constant level of risk reflects recognition by regulators that securities/derivatives held in the trading book are generally much more liquid than those in the banking book, where a buy-and-hold assumption over one year may be reasonable. It implies that IRC should be modeled under the assumption that banks rebalance their portfolio several times over the capital horizon in order to maintain a constant risk profile as market conditions evolve. Of course, we do not suggest that the constant level of risk framework be taken literally as a model of banks’ behavior: clearly portfolios are altered on a daily basis, not simply held constant for some period then instantaneously rebalanced. Rather, we regard the rollover interpretation as being a reasonable approximation to the way banks manage their trading portfolios over a certain horizon. In general, one should model constant level of risk instead of constant portfolio over one year capital horizon.

There are several ways to interpret constant level of risk: constant loss distribution or constant risk metrics (e.g. VaR). We believe the constant loss distribution assumption is the most rigorous. Under this assumption, the same metrics (e.g. VaR, moments, etc.) can be achieved for each liquidity horizon. 

The liquidity horizon for a position or set of positions has a floor of three months. Let us use three months as an example. We interpret constant level of risk to mean that the bank holds its portfolio constant for the liquidity horizon, then rebalances by selling any default, downgraded, or upgraded positions and replaces them so that the portfolio is returned to the level of risk it had at the beginning. The process is repeated 4 times over the capital horizon resulting 4 independent and identical loss distributions. The one year constant level of risk loss distribution is the convolution of 4 copies of the three month loss distribution. In Monte Carlo context, this can be modeled by drawing 4 times from the single period loss distribution measured over the liquidity horizon. The total PnL is the summary of these 4 random draws.

An intuitive explanation is shown in Figure 2. A generic path with appears in red; P&L contributions from each liquidity horizon appear in blue. In this schematic, the position experiences downgrade, upgrade or default, resulting in a loss or profit.  This position is then removed and replaced at the end of each liquidity horizon by rebalancing. The final P&L for the path will be the summary of all losses and profits.

References

Alessandro Aimone, 2018, “ING’s market risk charge edges higher,” Risk Quantum, 2018

Basel Committee on Banking Supervision, 31 July 2003, “The new Basel capital accord.”

Basel Committee on Banking Supervision, July 2008, “Guidelines for Computing Capital for Incremental Default Risk in the Trading Book.”

Basel Committee on Banking Supervision, July 2009 (a), “Guidelines for Computing Capital for Incremental Risk in the Trading Book.”

Basel Committee on Banking Supervision, July, 2009 (b), “Revisions to the Basel II market risk framework.” 

Basel Committee on Banking Supervision, October 2009 ©, “Analysis of the trading book quantitative impact study.”

Gary Dunn, April 2008, “A multiple period Gaussian Jump to Default Risk Model.”

FinPricing, Market Data Solution, https://finpricing.com/lib/IrCurveIntroduction.html

Erik Heitfield, 2003, “Dealing with double default under Basel II,” Board of Governors of the Federal Reserve System.

Jongwoo Kim, Feb 2009, “Hypothesis Test of Default Correlation and Application to Specific Risk,” RiskMetrics Group.

J.P.Morgan, April, 1997, “CreditMetrics – Technical Document.” 

Dirk Tasche, Feb 17, 2004, “The single risk factor approach to capital charges in case of correlated loss given default rates.” 

Tim Xiao, 2017, “A New Model for Pricing Collateralized OTC Derivatives.” Journal of Derivatives, 24(4), 8-20.



